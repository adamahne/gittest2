// COPYRIGHT 2010-2014 Adam Ahne
// all rights reserved
// use of this program is limited to True Dungeon / True Adventure
// this code may be modified for use by True Dungeon / True Adventure
// compile with HI-TECH C compiler (free from microchip.com with MPLAB install)
// new comment
// comment in master git path
// added a comment in testing
// added another master comment

/* True Dungeon Borg (hardware V3)
control AC devices via an off-the-shelf relay box, controls DMX lighting, and controls a DVD player via IR remote control

microcontroller: PIC16F1827 (Microchip brand)

pin14: VDD 5V
comment in testing
pin5: Gnd
pin17: RA0, input (analog/compare), spare
pin18: RA1, output, status LED 
pin1: RA2, input, zener clamped external contact closure (also DAC out)
pin2:  RA3, output (make it an input to drive it low via external pulldown), IR out (PWM)
pin3:  RA4, output, relay driver output enable (PWM)
pin4:  RA5, _MCLR, for debugger
pin15:  RA6, in/out, serial data in/out
pin16:  RA7, in, zener clamped external contact closure


pin6: RB0, in, internal pullup, zero the RTC, (also PWM)
pin7:  RB1, input, IR input (capture)
pin8:  RB2, in/out, internal pullup, mode select / DMX out
pin9:  RB3, in/out, internal pullup, mode select / latch relay driver/ also run to expansion board
pin10:  RB4, out, serial clock
pin11:  RB5, in/out, internal pullup, mode select / latch RealTimeClock
pin12:  RB6, ICSPCLK, for debugger
pin13:  RB7, ICSPDAT, for debugger


Timers:
TMR0	8uS, general timekeeping
TMR1	25mS loop timing
TMR2	PWM timebase
TMR4	16uS, IR receiver timekeeping
TMR6	1uS, serial to DMX



*/

/*
Summary of program structure

defines: 
	version number
	IR commands sent by the GM remote control and received by the Borg receiver
	the states for the state machine
	the DMX effects
	relay time constants
	DVD player remote control bit sequences

global variables

lots of little helper functions like delayMS, send data to serial devices, etc.

interrupt routine.  This is a background process that receives IR commands from the GM remote

main.  This is the main program loop



main is summarised below:

	call the initialization routine to initialize everything
	check if, on power-up, the Borg jumper is shorted.  If it is, zero the Real Time Clock and sit and rapidly blink the LED until the jumper is removed
	turn on the DVD player and wait for it to be ready
	if 2nd dip switch is open, play track 6 (sound check).  If 2nd dip switch is closed, skip the sound check
		
	main loop (loops every 25mS)
		
		once per second (as counted by the RealTimeClock):
			decrement any ongoing countdowns
			if dip switch 1 is open, check if we're at a 12 minute boundary so it's time to tell the players to get moving.  If dip switch 1 is closed, skip BGYAM warning
			if the Borg button is pressed, transmit the current RTC time so another Borg can sync.  Note that the Borg button should not be held down while powering up or this Borg will reset its RTC (button is the same as the jumper)
			toggle the Borg LED i.e. LED will be on for one second, off for one second, etc.	
		
		process any active DMX effects (see below for a detailed description of effects)
		mirror DMX channels to multiple DMX addresses
		set DMX debug data 
		transmit all DMX data.  Every DMX address is sent every 25mS regardless of whether a DMX device is listening.
		
		process the state:
			IDLE: 
				if a countdown ended, then change state to DEFAULT
				if an IR command was received, change state based on the command received:
					POWER button = DEFAULT state
					MUTE button = MUTE state
					CHANNEL_UP button = CH_PLUS state
					CHANNEL_DOWN button = CH_MINUS state
					VOLUME_DOWN = CH_MINUS state
					VOLUME_UP = CH_PLUS state
				if the first contact closure is closed, change state to CONTACT1
				if the second contact closure is closed, change state to CONTACT2
									
			CH_PLUS:
				play DVD USB track 3
				turn on relay 3 and relay 4 (does nothing if the relay box is not connected)
				turn on DMX channel 3 (does nothing unless a DMX dimmer box is connected and set to address 3)
				change state to WAIT_BEFORE_RELAY to give the DVD player time to react
		
			CH_MINUS:
				play DVD USB track 4
				turn on relay 5 and relay 6 
				turn on DMX channel 4 
				change state to WAIT_BEFORE_RELAY to give the DVD player time to react
		
			MUTE:
				play DVD USB track 5
				turn on relay 7 and relay 8 
				turn on DMX channel 5 
				change state to WAIT_BEFORE_RELAY to give the DVD player time to react
		
			WAIT_BEFORE_RELAY:
				if enough time has passed:
					turn on the proper relays.
					set-up DMX effects.  This is where you tweak the CH_PLUS, CH_MINUS, and MUTE DMX effects.
					kick-off DMX effects.
					change state to IDLE
		
			CONTACT1:
				change state to CH_PLUS (i.e. closing contact 1 is the same as sending a CH_PLUS on the GM remote)
		
			CONTACT2:
				change state to BGYAM.  This is used for Borg debug to test the BGYAM lights and sounds.
		  			
			BGYAM:
				play DVD USB track 2 (better get your ass moving soundtrack)
				turn on relay 1 and relay 2
				set-up BGYAM DMX effects.
				kick-off DMX effects.
				change state to IDLE
		
			SOUND_CHECK:
				every 15 seconds, play DVD USB track 6
				turn on relay 1 and relay 2
				turn on the green DMX lights	
				if any IR command is received from the GM remote, exit the sound check and change state to IDLE to process the IR command			
			DEFAULT:
				play DVD USB track 1
				turn on relay 1 and relay 2
				set-up DMX effectss
				kick-off DMX effects
				change state to IDLE
	
	end of main loop


*** Beginning of comments regarding DMX effects ***


You can have up to six DMX effects running at one time.  The job of an effect is to change a single byte in the DMXData[] array.  Since the whole DMXData[] array is transmitted to all DMX devices every 25mS, changing a byte in DMXData will change the DMX device that is set to the corresponding DMX address.  Note that the DMX address on the device is one larger than the index into DMXData[] array.  Thus, if a American DJ P36LED spotlight is configured for DMX address 8 (using dip switches on the spotlight), and a 126 is written to DMXData[7], the red LEDs in the spotlight will be set to half intensity (126/255 percent) because red is the 1st address for the P36LED and 126 is half the available power.

The elements of an effect control when to change the DMXData, how much to change it, and when to stop.  Calling clearEffect() resets all six effects to their default values.

Each effect has the following elements.  

	channel:  	offset into DMXData for this effect.  This corresponds to a DMX device address of channel+1
			default value is 7 for effect[0], 8 for effect[1], etc.
			Note that changing the value of DMXData[channel] is the whole purpose of this effect.

	action:	what action to take for this channel (see below for the list of actions)
			default value is NOTHING

	min:	the minimum DMXData[] value for this effect
			default value is 0

	max:	the maximum DMXData[] value for this effect
			default value is 255

	tick:	tick is a counter that is decremented every 25mS.  When tick is decremented to 0, tick is set to the variable called pause (see below).  Also, when tick is decremented to 0, DMXData[channel] is incremented or decremented by the variable called jump (see below).
			default value is 1
   			initialize tick with a large value to delay when the effect starts
   			
	pause:	a variable in units of 25mS.  When tick is decremented to 0, tick is set to pause.  Thus, tick will decrement to 0 every (pause * 25mS) seconds.
			default value is 5

	jump: 	how big of a jump to take in the DMXData[] value for each time tick is decremented to 0
			default value is 1

	death: 	a counter that is decremented every 25mS.  When death is decremented to 0, set DMXData[channel] to min and kill this effect by setting action to NOTHING.	
			default value is 0
   			setting death to 0 will make this effect live forever
   			Note that you can have two effects pointing to the same DMX address.  You can have the second effect wait to begin until the first effect finishes by setting death of the first effect and tick for the second effect. 

	bouncePause:	how many 25mS cycles to wait at min, after a bounce cycle, before bouncing again
				default value is 1


List of actions:

	NOTHING:
	Do nothing.
	
	FADE_ON:
	Take whatever value DMXData[channel] starts with and slowly increment it until it reaches the max for this effect.  Keep it at max forever.
	
	FADE_OFF:
	Take whatever value DMXData[channel] starts with and slowly decrement it until it reaches the min for this effect.  Keep it at min forever.
	
	PRELOAD_FADE_ON:
	Initialize DMXData[channel] to min for this effect.  Slowly increment DMXData[channel] until it reaches the max for this effect.  Keep it at max forever.
	
	PRELOAD_FADE_OFF:
	Initialize DMXData[channel] to max for this effect.  Slowly decrement DMXData[channel] until it reaches the min for this effect.  Keep it at min forever.
	
	BOUNCE_ON: 
	Take whatever value DMXData[channel] starts with and slowly increment it.  Once DMXData[channel] reaches the max for this effect, change the action to BOUNCE_OFF.  
	
	BOUNCE_OFF:
	Take whatever value DMXData[channel] starts with and slowly decrement it.  Once DMXData[channel] reaches the min for this effect, wait based on bouncePause, then change the action to BOUNCE_ON.  
	
	FLICKER:
	A special case of bounce that alternates DMXData[] between min and max for this effect.  The time DMXData[] spends at min is based on bouncePause.  The time DMXData[] spends at max is based on pause.

The default True Dungeon room will use DMX address 8 for the LED spotlight.  The alternate True Dungeon rooms will use DMX address 16 for the LED spotlight.  Some effects will be common between the two room types.  Instead of having redundant effects, you can mirror the DMXData[] for, say, DMX address 8 to DMX address 16.  The mirroring is defined in the mirror[] array.  If mirror[x] contains a non-zero value, DMXData[x] = DMXData[mirror[x]].

*** end of comments regarding DMX effects ***







*/





/*

#include <htc.h>
// config for old hi-tech C compiler
__CONFIG(0xFE4);	// program config word 1
__CONFIG(0x1013);	// program config word 2
*/

#include <htc.h>
// config for new XC8 compiler
__PROG_CONFIG(1, 0xFE4);	// program config word 1
__PROG_CONFIG(2, 0x1013);	// program config word 2




#define bitset(var, bitno) ((var) |= 1UL << (bitno))	// set bit
#define bitclear(var, bitno) ((var) &= ~(1UL << (bitno)))	// clear bit





// these IRCommand defines are based on the Magnavox remote set to 003
#define	POWER			32
#define	MUTE_BUTTON		33
#define	CHANNEL_UP		129
#define CHANNEL_DOWN	128
#define VOLUME_DOWN		136
#define VOLUME_UP		144 


// state defines
#define IDLE 	0
#define DEFAULT 1
#define CH_PLUS 2
#define CH_MINUS 3
#define WAIT_BEFORE_RELAY 4
#define CONTACT1 5
#define CONTACT2 6
#define MUTE 7
#define BGYAM 8
#define SOUND_CHECK 9
#define RTC_WAS_SET 10
#define V_MINUS 11
#define V_PLUS 12
#define WARNING 13


// effect defines
#define NOTHING 0
#define FADE_ON	1
#define FADE_OFF 2
#define BOUNCE_ON 3
#define BOUNCE_OFF 4
#define FLICKER 5
#define PRELOAD_FADE_ON 6
#define PRELOAD_FADE_OFF 7



/*
FYI Here's how I emulate an IR remote control.  I take an IR detector and hook it up to an oscilliscope.  I press, say, the power button on the IR remote control and capture the detector's waveform.  These things spit out a stream of digital pulses of a set width and pattern for each button press. 
 
After capturing the waveform, I figure out what the time base is i.e. a unit of time that all digital pulses (on and off times) are an integer multiple of.  For the Philips DVD player the time base is 0.44mS.  I then figure out what stream of 1s and 0s represents the waveform, assuming a 1 means "send light for 0.44mS" and a 0 means "don't send light for 0.44mS".
 
The power button looks like this:
11111100 10010101 11000101 01010110 01011010 01010110 10100000
 
 
Here's the part of the code that stores the definitions for the buttons:
*/


#define COMMAND_LENGTH 15


// table of IR commands
// device type, command number, command data

    
const unsigned char IRCOMMAND[1][15][COMMAND_LENGTH] = {
252, 149, 197, 86, 90, 86, 160, 0, 0, 0, 0, 0, 0, 0, 0, // Philips HTS3371D power 
252,149,197,86,86,170,144,0,0,0,0,0,0,0,0, // 1 usb 
252,149,53,86,86,106,80,0,0,0,0,0,0,0,0, // 2 ok
252,149,197,86,85,85,96,0,0,0,0,0,0,0,0, // 3 "1"
252,149,53,86,85,85,144,0,0,0,0,0,0,0,0, // 4 "2"
252,149,197,86,85,85,160,0,0,0,0,0,0,0,0, // 5 "3"
252,149,53,86,85,86,80,0,0,0,0,0,0,0,0, // 6 "4"
252,149,197,86,85,86,96,0,0,0,0,0,0,0,0, // 7 "5"
252,149,53,86,85,86,144,0,0,0,0,0,0,0,0, //8 "6"  
252,149,197,101,85,101,80,0,0,0,0,0,0,0,0, // 9 vol+
252,149,53,101,85,101,96,0,0,0,0,0,0,0,0, // 10 vol- 
252,149,197,101,86,101,144,0,0,0,0,0,0,0,0, // 11 surr
252,149,53,86,85,86,160,0,0,0,0,0,0,0,0, //12 "7"  
252,149,197,86,85,89,80,0,0,0,0,0,0,0,0, //13 "8"  
0b11111100,0b10010101,0b00110101,0b01010110,0b01010101,0b01011001,0b01100000,0,0,0,0,0,0,0,0 //13 "9"  


		/* removed to conserve flash space
		255, 0, 136, 170, 138, 34, 162, 138, 42, 170, 162, 34, 34, 34, 0, // Samsung 0 power
		255, 0, 136, 170, 138, 34, 162, 138, 40, 170, 168, 162, 34, 34, 0, // 1 enter  
		255, 0, 136, 170, 138, 34, 162, 138, 42, 168, 168, 136, 136, 162, 0, // 2 "1" 
		255, 0, 136, 170, 138, 34, 162, 138, 138, 168, 162, 136, 136, 162, 0, // 3 "2"  
		255, 0, 136, 170, 138, 34, 162, 138, 34, 170, 42, 136, 136, 162, 0, // 4 "3"  
		255, 0, 136, 170, 138, 34, 162, 138, 162, 168, 162, 40, 136, 162, 0, // 5 "4"  
		255, 0, 136, 170, 138, 34, 162, 138, 40, 170, 42, 40, 136, 162, 0, // 6 "5"  
		255, 0, 136, 170, 138, 34, 162, 138, 136, 170, 40, 168, 136, 162, 0, // 7 "6"  
		255, 0, 136, 170, 138, 34, 162, 138, 136, 168, 168, 168, 138, 34, 0, // 8 info  
		255,0,136,170,138,34,162,138,40,136,170,138,162,34, 0, // 9 right arrow  
		255, 0, 136, 170, 138, 34, 162, 138, 162, 34, 168, 138, 162, 34, 0, // 10 return  
		
		
		246,181,170,171,90,0,0,0,0,0,0,0,0,0,0, // new Sony 0 power  
		245,109,182,170,213,90,160,0,0,0,0,0,0,0,0, // new Sony 1 enter  
		245,85,85,90,171,84,0,0,0,0,0,0,0,0,0, // new Sony 2 "1" 
		246,170,170,173,85,170,0,0,0,0,0,0,0,0,0, // new Sony 3 "2"  
		245,170,170,173,85,170,0,0,0,0,0,0,0,0,0, // new Sony 4 "3"  
		246,213,85,86,170,213,0,0,0,0,0,0,0,0,0, // new Sony 5 "4"  
		245,106,170,173,85,170,0,0,0,0,0,0,0,0,0, // new Sony 6 "5"  
		246,181,85,86,170,213,0,0,0,0,0,0,0,0,0, // new Sony 7 "6"  
		245,181,85,86,170,213,0,0,0,0,0,0,0,0,0, // new Sony 8 "7"  
		246,218,170,171,85,106,128,0,0,0,0,0,0,0,0, // new Sony 9 "8"  
		246,181,170,171,90,0,0,0,0,0,0,0,0,0,0 // new Sony 10 nothing  
		
		*/


};

 

const struct {
   unsigned char BASE;  // number of TMR0 counts per transmission unit
                        // 8uS per TMR0 count
   unsigned char CARRIER;  // PWM carrier frequencies generated by PWM module
				// number of system clocks -1
                // 99 is 40kHz
				// 105 is 38kHz
} IRCONSTANT[3] = { /* Philips HTS3371D */     55, 105, 
				/* Samsung DVD player */ 70, 105,
				/* new Sony DVD player */     72, 99 };

 

//========================================================================================
// Globals
//========================================================================================
unsigned char mode;	// holds the POR state of the mode dip switches
					// left dip switch: skip DVD POR
					// middle dip switch: leach i.e. RTC clock setting mode
					// right dip switch: skip BGYAM
unsigned char IRCommand;	// holds the latest IR command received from IR remote
unsigned char second;		// from RTC
unsigned char minute;		// from RTC
unsigned char DVDPlayer;	// sets the type of DVD player. 0==Philips, 1==Samsung

unsigned char collision;	// track i2c bus collisions
unsigned char bridgeInstalled=0;	// TRUE if bridge was detected during init

unsigned char EEPROMBuffer[32];	// buffer for EEPROM writes


/* contents of DMXData
Keep in mind that C starts at address 0 and the DMX lights start at address 1 so the DMX address will be 1 greater than the array index
	[0]
	[1] 
	[2] 
	[3] 
	[4]
	[5]
	[6]
	[7] American DJ P36LED red
	[8] American DJ P36LED green
	[9] American DJ P36LED blue
	[10] American DJ P36LED reserved
	[11] American DJ P36LED reserved
	[12] American DJ P36LED reserved
	[13]
	[14]
	[15] American DJ P36LED red
	[16] American DJ P36LED green
	[17] American DJ P36LED blue
	[18] American DJ P36LED reserved
	[19] American DJ P36LED reserved
	[20] American DJ P36LED reserved
	[21]
	[22]
	
	[23] 
	[24] 
	[25] 
	[26] 
	[27] 
	[28] 
	[29] 
	[30] default for the virtual DMX address of the relay pack
	[31]



*/

#define MAXDMX 32
unsigned char DMXData[MAXDMX];	// hold up to MAXDMX DMX outputs
unsigned char mirror[sizeof(DMXData)];	// channels to mirror DMX outputs


#define MAX_EFFECT 6

struct effectStruct {
   unsigned char channel;  // offset into DMXData for this effect
   unsigned char action;	// what action to take for this channel (see defines for a list of actions)
   unsigned char min;	// the minimum output value for this effect
   unsigned char max;	// the maximum output value for this effect
   unsigned char pause;	// how many 25mS ticks between doing something for this effect
   unsigned char jump;	// how big of a jump to take in the DMX value for each time we do something
   unsigned int tick;	// decrement every 25mS for timing within this effect
   						// initialize tick with a large value to delay when the effect starts
   unsigned int death; // how many times we do something for this effect before setting DMX value to min and killing this effect
   						// setting death to 0 will make it live forever
   unsigned int bouncePause;	// how many ticks to wait after a bounce cycle before bouncing again

} effect[MAX_EFFECT];




// before including the .h file, define any required functions that are in the .c file
void clearDMX(void);
unsigned char readEEPROM(unsigned int readAddress);



//****************************************************************************************
//====================== Here lies the include for the effect defines ====================
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#include "effect defines 2014 EEPROM.h"
//#include "c:\tmp\effect defines.h"




//========================================================================================
// provide a delay in ms
//========================================================================================
void delayMS(unsigned char ms)
{
	unsigned char i;
	for(i=0;i!=ms;i++)	// wait for ms milliseconds
	{
		TMR0=0;	// clear TMR0 for delay
		while(TMR0 < 125);	// wait 1mS
	}
}


//========================================================================================
// provide a delay in seconds
//========================================================================================
void delayS(unsigned char s)
{
	unsigned char i,j;
	for(i=0;i!=s;i++)	// wait for s seconds
	{
		for(j=0;j!=5;j++)
		{
			delayMS(200);
		}
	}
}



//========================================================================================
// provide a short delay
//========================================================================================
void delayShort(void)
{
	TMR0=0;	// clear TMR0 for delay
	while(TMR0 < 1);	// wait
}



//========================================================================================
//	bit bang a byte out to a serial device, LSB first
//========================================================================================
void BB_write(unsigned char dat)
{	
	char x;	

	bitclear(TRISA,6);	// make serial data pin an output

	for(x=0;x<8;x++)	
	{		
		if(dat & 0x01)			//put bit on data pin		
		{			
			bitset(PORTA,6);		
		} else bitclear(PORTA,6);
		
		delayShort();	// wait 
		bitset(PORTB, 4);				//toggle clock to latch bit		
		delayShort();	// wait 
		bitclear(PORTB,4);		
		dat >>= 1;			//move next bit into place	
	}
}


//========================================================================================
//	bit bang a byte in from a serial device, LSB first
//========================================================================================
unsigned char BB_read(void)
{	
	char x;	
	unsigned char dat;	
	dat = 0;	

	bitset(TRISA,6);	// make serial data pin an input
	
	for(x=0;x<8;x++)	
	{		
		dat >>= 1;		// shift the bits

		delayShort();	// wait 		

		if(PORTA & 0b1000000)				//read a bit		
		{			
			dat |= 0x80;	// set the MSB		
		} 		
		bitset(PORTB, 4);				//toggle clock to advance to next bit
		delayShort();	// wait 		
		bitclear(PORTB,4);		
			
	}	

	return(dat);
}

//========================================================================================
//	read or write a byte to the DS1302 real time clock
//	address is address in DS1302 memory map, includes address header bits
//  data is data to be written
//========================================================================================
unsigned char RTC(unsigned char address, unsigned char data)
{	
	unsigned char buff=0;
	
	// send the address header
	bitset(PORTB,5);	// enable RTC communication
	BB_write(address);	// send the address header

	if(address & 1)
	{
		buff=BB_read();
	} else
	{
		BB_write(data);
	}

	bitclear(PORTB,5);	// disable RTC communication

	return(buff);

}


//========================================================================================
//	write to the relay driver
// 	note: MSB and LSB will no longer be swap i.e. data will not be sent backward
//========================================================================================
void relay(unsigned char data)
{

	int buff;
	
	// swap the bit order
	buff=0;
	if(data & 0b00000001) buff=buff | 0b10000000;
	if(data & 0b00000010) buff=buff | 0b01000000;
	if(data & 0b00000100) buff=buff | 0b00100000;
	if(data & 0b00001000) buff=buff | 0b00010000;
	if(data & 0b00010000) buff=buff | 0b00001000;
	if(data & 0b00100000) buff=buff | 0b00000100;
	if(data & 0b01000000) buff=buff | 0b00000010;
	if(data & 0b10000000) buff=buff | 0b00000001;


	BB_write(buff);	// send the output byte
	delayShort();		// give it time to settle
	bitset(PORTB,3);	// latch in the data
	delayShort();		// give it time to settle
	bitclear(PORTB,3);	// latch should end low

}


//========================================================================================

 

//---------------------------------------------------------------------------
// send a single IR button press
// IRcommand: index into IRCOMMAND
//---------------------------------------------------------------------------

void SendIR(unsigned char command)
{
   unsigned char i,j;
   unsigned char q;
   unsigned char buf;
   unsigned char increment_tmr0;
   unsigned char cmd[COMMAND_LENGTH]; // hold the command
   unsigned char z;


	if(mode & 0b10 ) return; // do nothing if in leach mode


   	increment_tmr0=IRCONSTANT[DVDPlayer].BASE; // load the time base
	
	// start PWM at correct frequency //
	
	PR2=IRCONSTANT[DVDPlayer].CARRIER;	// load the PWM period


	// set retrys
	if(DVDPlayer==1) z=1;
	else if(DVDPlayer==2) z=4;
	else if(DVDPlayer==0) z=2;


    di(); // disable interrupts

	// loop, send command multiple times
	for(q=0;q!=z;q++)
	{
	   // copy the command to the cmd buffer
	   for(i=0;i!=COMMAND_LENGTH;i++) cmd[i]=IRCOMMAND[DVDPlayer][command][i];
	  
	  
	
	// initialize TMR0 next value
	   TMR0=255-increment_tmr0; // initialize TMR0
	
	
	   i=0;
	   // cycle through each command byte
	   while(i!=COMMAND_LENGTH)
	   {
	      buf=cmd[i]; // get the next byte of the command sequence
	     
	      // cycle through each bit of buf
	      j=8;
	      while(j)
	      {
			 while(TMR0 > 100); 	// loop until TMR0 rolls over
	         TMR0=TMR0-increment_tmr0;  // increment TMR0 next value
	         	
	         if(buf & 0b10000000)
	         {          
	         	bitclear(TRISA,3);         // enable PWM output
	         } else
	         {
				bitset(TRISA,3);		// disable PWM output
	         }
			 buf=buf<<1; // shift bits to the left
	         j--;
	      }
	      i++;
	   }
	   	if(DVDPlayer==0)
	   	{
			delayMS(50);	// stagger transmissions   
		}
		 
	}

    ei(); 


	// delay to let the player respond
	delayMS(50);


}


//---------------------------------------------------------------------------
// transmit a zero to another TDBoard receiver
//---------------------------------------------------------------------------
void transmitZero(void)
{
		bitclear(TRISA,3);	// enable PWM output
		TMR0=0;		// set up the delay
		while(TMR0 < 62);
		bitset(TRISA,3);	// disable PWM output
		TMR0=0;		// set up the delay
		while(TMR0 < 62);
		
}

//---------------------------------------------------------------------------
// transmit a one to another TDBoard receiver
//---------------------------------------------------------------------------
void transmitOne(void)
{
		bitclear(TRISA,3);	// enable PWM output
		TMR0=0;		// set up the delay
		while(TMR0 < 150);
		bitset(TRISA,3);	// disable PWM output
		TMR0=0;		// set up the delay
		while(TMR0 < 62);
}


//---------------------------------------------------------------------------
// transmit to another TDBoard receiver
//---------------------------------------------------------------------------
void transmit(char data)
{

	int i;

	PR2=105;	// load the PWM period for 38kHz

	// send the three-bit header
/*	for(i=0;i!=3;i++)
	{
		transmitZero();
	}
*/
	transmitZero(); // takes less code space to do it this way
	transmitZero();
	transmitZero();

	// send the eight bits of data, MSB first
	for(i=0;i!=8;i++)
	{
		if(data & 0x80)
		{
			transmitOne();
		} else
		{
			transmitZero();
		}
		data=data<<1;
	}


	// send the one-bit footer
	transmitZero();

	delayMS(55);	// give time to process command
}


 
//------------------------------------------------------------------------
// interrupt processes received IR commands
// interrupt on falling edge of RB1
//
// IR remote is Magnavox
// 38kHz carrier (filtered by IR receiver IC), inverted logic, 0==off time < 0.84mS, 1==off time > 0.84mS
// use 16uS timebase with TMR4
// Received value is stored in global variable IRCommand
// 	power:	32
//	mute:	33
//	CH_PLUS:	129
//	CH_MINUS:	128
//	V_MINUS:		136
//	V_PLUS:		144 
//------------------------------------------------------------------------

void interrupt tc_int(void)
{

	unsigned char buff=0;	// temporary buffer to hold received command bits
	unsigned char i;	
	unsigned char timeout=0;	// flag if a timeout occured

		
	for(i=0;i!=12;i++)
	{
		TMR4=0;	// clear TMR4 to allow while loop to timeout
		while((PORTB & 0x02) && (TMR4 < 100) );	// wait for input to go low
		if(TMR4 >= 100)
		{
			timeout=1;	// timeout occured
			break;	// exit loop
		}
		TMR4=0;	// clear TMR4 to start timing negative pulse width and allow while loop to timeout
		while(!(PORTB & 0x02) && (TMR4 < 120) );	// wait for input to go back high
		if(i> 2 && i < 11)	// ignore the first three bits and the last bit
		{
			buff=buff<<1;	// shift buff to make room for the next bit
			if(TMR4 > 52)
			{
				buff=buff | 0x01;	//received bit was a 1
			}
		}
	}


	if(buff != 0 && buff !=255 && timeout==0) IRCommand=buff;	// save the received command (if it was valid)



	bitclear(INTCON,0);	// clear flag
	bitclear(IOCBF,1);	// clear flag
   	PIR1=0; // clear interrupt flag
}


//------------------------------------------------------------------------
// set all DMX data to zero
//------------------------------------------------------------------------
void clearDMX(void)
{

	unsigned char i;
	
	for(i=0;i!=sizeof(DMXData);i++)	// initialize DMXData
	{
		DMXData[i]=0;
		mirror[i]=0;
	}


}

//------------------------------------------------------------------------
// set all effect actions to nothing
//------------------------------------------------------------------------
void clearEffect(void)
{
	unsigned char i;	
	
	for(i=0;i!=MAX_EFFECT;i++)	// initialize effect
	{
		effect[i].action=NOTHING;	// do nothing	
		
		// set some default values
		effect[i].channel=i+7;
   		effect[i].min=0;
   		effect[i].max=255;
   		effect[i].pause=5;
   		effect[i].jump=1;
   		effect[i].tick=1;
   		effect[i].death=0;	// live forever
   		effect[i].bouncePause=1;
	}
}


//--------------------------------I2C code starts here ------------------------------------------
// talks to the I2C RTC in the bridge, if present
//===============================================================================================
// I2C defines
//===============================================================================================

#define SCL     RB4 	// I2C bus clock
#define SDA     TRISA0 //
//#define SCL_IN  RB6    //
#define SDA_IN  RA0    //



//We use a small delay routine between SDA and SCL changes to give a clear sequence on the I2C bus. This is nothing more than a subroutine call and return.
void i2c_dly(void)
{
	if(PORTA & 0b1000000) collision=1;  // check for bus collision
	if(PORTA & 0b1000000) collision=1;  // check for bus collision
}

//The following 4 functions provide the primitive start, stop, read and write sequences. All I2C transactions can be built up from these.
void i2c_start(void)
{
  SDA = 1;             // i2c start bit sequence
  i2c_dly();
  SCL = 1;
  i2c_dly();
  i2c_dly();
  i2c_dly();
  SDA = 0;
  i2c_dly();
  SCL = 0;
  i2c_dly();
  
}

void i2c_stop(void)
{
	int i;
	
	// verify that data is high before trying to assert a stop condition.  If low, send clocks until high
  SDA=1;	// make data an input
  i2c_dly();	// wait for the line to transition
  
	// this loop not required but left in for added robustness.  It should always immediately exit the loop
  for(i=0;i!=12;i++)	// set max additional clocks to clear input
  {
	if(SDA_IN!=0) break;	// exit if data is high
	SCL=1;
	i2c_dly();	
	SCL=0;
	i2c_dly();	  
  } 
  

  SDA = 0;             // i2c stop bit sequence
  i2c_dly();
  SCL = 1;
  i2c_dly();
  SDA = 1;
  i2c_dly();
  
}

unsigned char i2c_rx(char ack)
{
  unsigned char x, d=0;
  SDA = 1; 
  i2c_dly();
  for(x=0; x<8; x++) {
    d <<= 1;
	SCL=1;
	i2c_dly();
    if(SDA_IN) d |= 1;
    SCL = 0;
    i2c_dly();
  } 
  if(ack) SDA = 0;
  else SDA = 1;
  i2c_dly();
  SCL = 1;
  i2c_dly();             // send (N)ACK bit
  SCL = 0;
  i2c_dly();
  SDA = 1;
  i2c_dly();
  

  if(collision) d=0;
  
  return d;
  
}

bit i2c_tx(unsigned char d)
{
unsigned char x;
static bit b;
  for(x=8; x; x--) {
    if(d&0x80) SDA = 1;
    else SDA = 0;
 	i2c_dly();
    SCL = 1;
    i2c_dly();
    d <<= 1;
    SCL = 0;
    i2c_dly();
  }
  
  SDA = 1;
  i2c_dly();
  SCL = 1;
  i2c_dly();
  b = SDA_IN;          // possible ACK bit
  SCL = 0;
  i2c_dly();
  
//  if(PORTA & 0b1000000) collision=1;  // check for collision

  return b;
}



//------------------------------------------------------------------------
// read I2C clock
// sets global variables second and minute
//------------------------------------------------------------------------
void readI2CClock(void)
{
	
	//initialize the ports 
	bitclear(PORTA,6);	// drive mux select lines low, if porta is an ouput (pullups will take care of it if already input)
	SDA = 1;
	SCL = 1;
	SDA_IN = 0;
	i2c_dly();

	
	i2c_stop();
	i2c_dly();	
	
	
	// i2c read block
	i2c_start();              // send start sequence
	i2c_tx(208);          // I2C write address
	i2c_tx(0);             // set RTC register pointer.  reading will start from this address
							// seconds are address 0, minutes are address 1
	i2c_stop();               // send stop sequence		

	
	i2c_start();              // send start sequence
	i2c_tx(208 + 1);          // I2C read address

	second=i2c_rx(1);	// get the second value from bridge RTC

	minute=i2c_rx(0);		// get the minute value.  No ack

	i2c_stop();	// close out by sending a stop sequence
	i2c_stop();	


}

//------------------------------------------------------------------------
// write I2C clock
// sends global variables second and minute
//------------------------------------------------------------------------
void writeI2CClock(void)
{
	
	//initialize the ports 
	bitclear(PORTA,6);	// drive mux select lines low, if porta is an ouput (pullups will take care of it if already input)
	SDA = 1;
	SCL = 1;
	SDA_IN = 0;
	i2c_dly();
	i2c_stop();
	i2c_dly();	
	
	
	// i2c write block
	i2c_start();              // send start sequence
	i2c_tx(208);          // I2C write address
	i2c_tx(0);             // set RTC register pointer.  writing will start from this address
	i2c_tx(second);
	i2c_tx(minute);
	i2c_stop();               // send stop sequence		
	i2c_stop();	



	
}



//------------------------------------------------------------------------
// writes EEPROMBuffer to the specified writeAddress
// note that writeAddress must be on a 32 byte boundary due to limitations of the EEPROM
void writeEEPROM(unsigned int writeAddress)
{
	unsigned int j;
	unsigned char k;
	unsigned char i;

	//initialize the ports 
	bitclear(PORTA,6);	// drive mux select lines low, if porta is an ouput (pullups will take care of it if already input)
	SDA = 1;
	SCL = 1;
	SDA_IN = 0;
	i2c_dly();
	i2c_stop();
	i2c_dly();	
	
	
	// i2c EEPROM write block
	i2c_start();              // send start sequence
	i2c_tx(0b10100000);          // I2C write address
	j=writeAddress >> 8;	// get MSByte
	k=j;
	i2c_tx(k);             // high address byte
	j=writeAddress & 0xff;	// get LSByte
	k=j;
	i2c_tx(k);			// low address byte
	
	// send EEPROMBuffer
	for(i=0;i!=sizeof(EEPROMBuffer);i++)
	{
		i2c_tx(EEPROMBuffer[i]);	// transmit the byte	
	}	
	
	i2c_stop();               // send stop sequence		
	i2c_stop();	

	
}	

/*
//------------------------------------------------------------------------
// poll EEPROM to see if it is still busy with a write
// return 0 if not busy, else return 1
unsigned char pollEEPROM(void)
{
	return(0);	//	TODO: add polling code 
}	
*/

//------------------------------------------------------------------------
// reads from EEPROM at the specified readAddress
// reads 32 bytes, starting at readAddress, and stores them in EEPROMBuffer
// also returns the value of the first byte read i.e. the byte at readAddress
unsigned char readEEPROM(unsigned int readAddress)
{
	unsigned int j;
	unsigned char k;
	unsigned char i;
	unsigned char val;

	//initialize the ports 
	bitclear(PORTA,6);	// drive mux select lines low, if porta is an ouput (pullups will take care of it if already input)
	SDA = 1;
	SCL = 1;
	SDA_IN = 0;
	i2c_dly();
	i2c_stop();
	i2c_dly();	
	
	
	// i2c EEPROM write to set the word address
	i2c_start();              // send start sequence
	i2c_tx(0b10100000);          // I2C write address
	j=readAddress >> 8;	// get MSByte
	k=j;
	i2c_tx(k);             // high address byte
	j=readAddress & 0xff;	// get LSByte
	k=j;
	i2c_tx(k);			// low address byte
	
	i2c_start();	// send another start sequence
	i2c_tx(0b10100001);	// read

	val=i2c_rx(1);	// read byte, with ack
	EEPROMBuffer[0]=val;	// save first byte
	for(i=1;i!=31;i++) EEPROMBuffer[i]=i2c_rx(1);	// read bytes into EEPROMBuffer
	EEPROMBuffer[31]=i2c_rx(0);	// no ack on the last read
	
	i2c_stop();	

	return(val);


}

//========================================================================================
// process the raspberry pi command to the bridge
// note: the raspi must keep control of the bridge for at least 10mS to be sure we detect it
void processRaspi(void)
{
	unsigned char raspiCommand;
	unsigned char i,x,y;
	unsigned int EEPROMAddress;
	static unsigned char transmissionNumber=0;	// increment every read data from EEPROM to bridge.  Used for raspi error detection

//TODO: report borg status i.e. last two events
//TODO: let raspi trigger events
	
	while(PORTA & 0b01000000);	// wait for raspi to give up the bridge

	
	//initialize the ports 
	bitclear(PORTA,6);	// drive mux select lines low, if porta is an ouput (pullups will take care of it if already input)
	SDA = 1;
	SCL = 1;
	SDA_IN = 0;
	i2c_dly();

	i2c_stop();	// be sure things are clear
	i2c_dly();	
	
	
	// i2c read block
	i2c_start();    // send start sequence
	i2c_tx(208);    // I2C write address
	i2c_tx(8);      // set RTC register pointer.  reading will start from this address
	
	i2c_stop();     // send stop sequence		

	
	i2c_start();              // send start sequence
	i2c_tx(208 + 1);          // I2C read address

	raspiCommand=i2c_rx(1);	// get the command from the raspi
	x=i2c_rx(1);	// get address MSB
	y=i2c_rx(1);	// get address LSB
	EEPROMAddress=(x<<8) + y;		
	
	switch(raspiCommand)
	{
		case 0b11:	// read data from bridge and write it to EEPROM
			for(i=0;i!=sizeof(EEPROMBuffer);i++)
			{
				EEPROMBuffer[i]=i2c_rx(1);	// get a byte	
			}	
			i2c_rx(0);	// close out read with a nack
			i2c_stop();
			writeEEPROM(EEPROMAddress);	// write data to EEPROM
			break;
			
		case 0b1:	// read data from EEPROM and write it to bridge
			i2c_rx(0);	// close out existing read with a nack
			i2c_stop();
			i2c_dly();
/*
			// read the block from the EEPROM to the EEPROMBuffer			
			for(i=0;i!=sizeof(EEPROMBuffer);i++)	
			{
				EEPROMBuffer[i]=readEEPROM(EEPROMAddress++);	// get a byte						
			}
*/
			readEEPROM(EEPROMAddress);	// get 32 bytes

			// send EEPROMBuffer to the bridge
			
			i2c_stop();	// just in case
			i2c_dly();  // wait
				
			// i2c write block
			i2c_start();              // send start sequence
			i2c_tx(208);          // I2C write address
			i2c_tx(11);             // set RTC register pointer.  writing will start from this address
			for(i=0;i!=sizeof(EEPROMBuffer);i++)
			{
				i2c_tx(EEPROMBuffer[i]);	// send the data read from EEPROM
			}	
			i2c_tx(transmissionNumber);	// add a transmission number to the end for error checking
			transmissionNumber++;
			i2c_stop();               // send stop sequence		
			break;
		
		default:
			i2c_rx(0);	// close out read with a nack
			i2c_stop();
			break;
				
	}	

	
}	

//========================================================================================
void init(void)
{
	int i,j;
//	unsigned char tempSecond, tempMinute;

	// set oscillator to internal, 16MHz, giving a 4MHz instruction cycle
	OSCCON=0b01111010;	// internal, 16MHz clock

	// set weak pullups and TMR0 prescaler
	OPTION_REG = 0b00000100;	// enable global weak pullups (MSB cleared)
								// tmr0 prescaler enabled
								// 1/32 prescaler, thus TMR0 will increment every 8uS

	// disable analog inputs
	ANSELA=0;	// disable porta analog inputs
	ANSELB=0;	// disable portb analog inputs


	// set PORTA direction
	PORTA=0b10000;	// initialize portA
	TRISA=0b10101100;	// 1 is input, 0 is output

	// clear relay driver
	relay(0);	// set all relay outputs to 0
	bitclear(PORTA,4);	// enable relay outputs	



// read mode dip switches, then make those pins outputs 
	PORTB=0;	// initialize portB
	TRISB=0b00101111;	// 1 is input, 0 is output
	WPUB=0b00101101;	// enable selected weak pullups (1 is pullup)
	delayMS(100);	// give the inputs time to settle
	// read mode bits.  If the dip switch is open (0) the input will read 1 so invert when assigning to mode
	if(PORTB & 0b100)
	{
		mode=0;
	} else
	{
		mode=1;
	}
	if(!(PORTB & 0b1000))
	{
		//mode=mode | 0b10;
		mode=0b111;	// if you're the leach, skip everything
	}
	if(!(PORTB & 0b100000))
	{
		mode=mode | 0b100;
	}
	
	TRISB=0b00000011;	// 1 is input, 0 is output
	

// DEBUG CODE!!!!
// mode=0b111;	// TODO: debug code


	
/* init global variables */
	IRCommand=0;
 

// configure Timer1
	T1CON=0x21;	// turn on tmr1, increment every 1uS

// configure Timer2
	T2CON=0x04;	// turn on timer 2, no prescaler, no postscaler
  
// configure PWM
	CCP3CON=0b00001100;	// set CCP3 as PWM
	CCPR3L=30;	// set PWM duty cycle to about 30%
	CCPTMRS=0;	// all PWMs are based off Timer2

// configure Timer4
	T4CON=0b0111;	// turn on timer 4, 1/64 prescaler, no postscaler, increments ever 16uS


// configure Timer6
	T6CON=0b101;	// turn on timer 6, 1/4 prescaler, no postscaler, increments every 1uS



// check for bridge RTC
	readI2CClock();	// sets second and minute
	if(second==0xff)
	{
		// try again, just to be sure
		readI2CClock();
		if(second==0xff) bridgeInstalled=0;
		else bridgeInstalled=1;
	} else
	{
		bridgeInstalled=1;	
	}	

	// set RTC based on bridge RTC
	if(bridgeInstalled)
	{
			RTC(0x80,second);	// write updated value to RTC
			RTC(0x82,minute);	// write updated value to RTC
	}	

	// start real time clock (if it hasn't started already)
	RTC(0x8e,0);	// clear the write protect bit	
	second=RTC(0x81,0);	// read the seconds
	if(second & 0x80);	// read the clock halt flag
	{
		RTC(0x80,second & 0x7f);	// clear the clock halt flag (MSB)
	}
	RTC(0x90,0b10100101);	// enable the trickle charger, 1 diode drop, 2k source from Vcc
	second=RTC(0x81,0);	// read the seconds
	minute=RTC(0x83,0);	// read the minutes





	// initalize serial output (DMX)
	BAUDCON=0b00001000;	// 16 bit baud rate generator is used
	SPBRGL=15;	// set baud rate to 250kBPS
	SPBRGH=0;	// ""
	TXSTA=0b01100101;	// async, 9 bit (for 2 stop bits), transmit enabled, high baud rate, last bit sent is 1 (extra stop bit)


	bitclear(PORTA,1);	// turn off status LED
	delayS(3);	// wait a few seconds

	// only do DVD powerup if dip switch is not set
	if((mode & 0b100) == 0) 
	{
		// report firmware version
		// long blinks
		for(i=0;i!=((VERSION & 0xf0) >> 4);i++)
		{
			bitset(PORTA,1); // turn on status LED 
			delayS(1);	// wait one second
			bitclear(PORTA,1);	// turn off status LED
			delayS(1);  // wait one second
		}
	
		// short blinks
		for(i=0;i!=(VERSION & 0xf);i++)
		{
			bitset(PORTA,1); // turn on status LED 
			delayMS(200);
			bitclear(PORTA,1);	// turn off status LED
			for(j=0;j!=10;j++) delayMS(100);
		}
		//delay before leaving LED on
		delayS(1);  // wait one second
		bitset(PORTA,1);	// turn on status LED
	}
	



 
// configure interrupts
	bitset(IOCBN,1);	// interrupt on falling edge of RB1
	INTCON=0b11001000;	// enable interrupts on input pins
	



}





 

//------------------------------------------------------------------------
// main
//------------------------------------------------------------------------
main()
{
   	unsigned char state;
   	unsigned char i;
	unsigned int countdown;	// decremented once per second
	unsigned int countdownEnded;	// marks countdown end
	unsigned int countdownHold;	// holding register for countdown
	unsigned char oldSecond;	// holds prior second value
	unsigned char oldIRCommand;	// holds prior IR command
	unsigned char buff;
	unsigned int loopCount;	// decrements every loop at 25mS
	// unsigned char relayHold;	// holding register for relay value, used to make delay between DVD and relay
	// unsigned char DMXHold;		// holding register for DMX channel to set to 255, used to make delay
	unsigned char StateHold;	// holding register for state, used to make delay
	unsigned char oldContact1;	// holding register for the previous contact1 state
	unsigned char oldContact2;	// holding register for the previous contact2 state
	unsigned char tempSecond;	// buffer to hold received second 
	unsigned char tempMinute;	// buffer to hold received minute

	unsigned char oldRelay;	// prior value of relay	
	unsigned char BGYAMButtonCountdown;	// holds countdown to debounce BGYAM button




	
   	init();	// initialize



	clearDMX();	// initialize DMX
	
	clearEffect();	// initialize effect

	DVDPlayer = 0;	// set to Philips



	// check if RB0 is shorted on powerup, signaling zeroing of the RTC
	if(!(PORTB & 0x1))	// check if RB0 is shorted
	{
		// make sure it is not a glitch
		delayMS(10);	// wait
		if(!(PORTB & 0x1))
		{
			RTC(0x80,0);	// zero RTC seconds
			RTC(0x82,0);	// zero RTC minutes
			if(bridgeInstalled)
			{
				second=0;
				minute=0;
				writeI2CClock();	// update bridge RTC	
			}	
		}
		// wait until short is removed, rapidly blink LED while waiting
		while(!(PORTB & 0x1))
		{
			bitset(PORTA,1); // turn on status LED 
			delayMS(100);
			bitclear(PORTA,1);	// turn off status LED
			delayMS(100);
		}
	}

	// only do DVD powerup if dip switch is not set
	if((mode & 0b100) == 0) 
	{
		// wait for the DVD player power to come up
	 	delayS(20);	// wait
	
		// turn on DVD player
		if(DVDPlayer==1 || DVDPlayer == 0)
		{ 	
			SendIR(0);	// send power-button press
		} else if(DVDPlayer==2)
		{
			for(i=0;i!=1;i++)
			{
				SendIR(0);	// send it again	
			}
		}
	
	 
	  	// give the DVD player time to finish powering up
	 	delayS(20);	//wait
		
		// if loading a USB, give it time to access the first time
		// also, if Philips, set the correct surround sound mode 
		if(DVDPlayer==0)
		{
			// access the USB device
			SendIR(1);	// USB
			delayS(5);	// wait
			
			//start track 1
			SendIR(8);	// send "6"
			SendIR(2);	// send OK
			delayS(4);	// give it 2 seconds to start
			
			SendIR(1);	// USB
			SendIR(11);	// send <surr> three times to get 5.1 sound
			delayS(2);	//
			
			SendIR(1);	// USB
			SendIR(11);	// send <surr> three times to get 5.1 sound
			delayS(2);	//
			
			SendIR(1);	// USB
			SendIR(11);	// send <surr> three times to get 5.1 sound
			delayS(2);	//
					
		}
	}
	
	if(mode & 0b10)
	{
		state=DEFAULT; // if 2nd dip switch is set, skip sound check mode
	} else
	{
   		state=SOUND_CHECK;     // start in the sound_check state 
 	}  	


	countdown=0;	// nothing counting down right now
	oldSecond=second;	// initialize
	countdownEnded=0;	// initialize
	oldContact1=0;		// initialize
	oldContact2=0;		// 	
	oldIRCommand=0;		//
	oldRelay=1;
	BGYAMButtonCountdown=0;

	// ------------------------------------ main loop -------------------------------

/*
The main() loop cycles every 25mS (makes computing timing easier).  There's a bit of state code that controls the state 
of the system e.g. IDLE, CH_PLUS, etc.  There's a seperate bit of effect code that just controls the DMX data based on 
the data in the effect[] structure.  Both bits of code execute every 25mS.  The state code stuffs data into the effect[] 
structure and then generally sits in the IDLE state.  The effect code massages the DMX data until each effect dies 
regardless of the current state.  Of course, most states start by killing all effects and starting its own effects.

*/

	while(1)
   	{


		// use borg's rtc
		second=RTC(0x81,0);	// read the seconds
		minute=RTC(0x83,0);	// read the minutes
		

		// ----------------------------- do all the once-per-second stuff -------------
		// check if second just rolled over
		if(oldSecond != second)
		{

		
			// decrement countdown, check for countdown end
			if(countdown)
			{
				countdown--;
				if(!countdown)
				{
					countdownEnded=1;	// mark end of countdown
				}
			}
			if(BGYAMButtonCountdown)
			{
				BGYAMButtonCountdown--;	
			}	

			// check if it is time to tell the players to get moving
			if((second == 0) && (minute == 0x11 || minute == 0x23 || minute == 0x35 || minute == 0x47 || minute==0x59))
			{
				if(state!=SOUND_CHECK)
				{	
					if(!(mode & 0b1))	// check if BGYAM is enabled (dip switch 1 is off)
					{
						state=BGYAM;	// trigger better get your ass moving
					} else
					{
						state=DEFAULT;	// retrigger default
						countdown=60;	// signal another default retrigger in 60 seconds in case
										// default track is 11 minutes long
					}
				}	
			}	
			
			
			
			

			// check if RB0 is shorted, signaling that this board should transmit its RTC
			if(!(PORTB & 0x1) && (mode & 0b10 ))	// check if RB0 is shorted, only do if leach selected
			{
				buff=(second & 0b1111)<<4 | 0b0010;	// suffix of 0b0010 means seconds low byte
				transmit(0b101);		// time command header
				transmit(buff);
				buff=(second & 0b1110000) | 0b0110;	// suffix of 0b0110 means seconds high byte
				transmit(0b101);		// time command header
				transmit(buff);
				buff=(minute & 0b1111) << 4 | 0b1010;	// suffix of 0b1010 means minutes low byte
				transmit(0b101);		// time command header
				transmit(buff);
				buff=(minute & 0b1110000) | 0b1110;		// suffix of 0b1110 means minutes high byte
				transmit(0b101);		// time command header
				transmit(buff);
			
			} 

			// display time
			// displayTime();
		} 
		oldSecond=second;	// used to detect when second changes
		// finished with once per second tasks
		
		// check for emergency trigger of BGYAM by pressing the borg button
		if(!(PORTB & 0x1) && BGYAMButtonCountdown==0 && ((mode & 0b10)==0 )) // don't do this for the leach
		{
			RTC(0x80,0);	// write updated value to RTC
			RTC(0x82,0x11);	// write updated value to RTC
			if(bridgeInstalled)
			{
				second=0;
				minute=0x11;
				writeI2CClock();	// update bridge RTC	
			}	
			oldSecond=0;	//			
			state=BGYAM;				
			BGYAMButtonCountdown=30;	// you've got this many seconds to lift your finger off the button
		}	
		
        // toggle status LED
		buff=second & 0b1;
        if(buff==0)	
     	{
           	bitclear(PORTA,1); // turn off status LED 
        } else
        {
           	bitset(PORTA,1); // status LED at 100% intensity
        }

		// provide a 25mS loop time 
		while(TMR1H<98)
		{	// wait for TMR1 to increment 
			// here, we check if a raspi is connected to the bridge and it sent a data packet
			bitset(TRISA,6);	// make RA6 an input	
			delayShort();	// wait for RA6 to transition
			if(PORTA & 0b01000000) // did raspi take control of the bridge?
			{
				delayShort();	// wait
				if(PORTA & 0b01000000)	// double check to avoid glitches
				{
					// yes, the raspi sent something.  Wait for it to give up control of the bridge and then process the command
					processRaspi();
					break;	// stop waiting in this while loop because raspi could have had us tied up a long time
				}	
			}	
		}
		TMR1L=87;	// preset TMR1
		TMR1H=0;
		
		
		// decrement loop counter
		loopCount--;		
		
		
		// ------------------ do the effects --------------------------
		for(i=0;i!=MAX_EFFECT;i++)
		{
			if(effect[i].channel>=MAXDMX) continue;	// skip the effect if the channel is out of bounds
			if(effect[i].action==NOTHING) continue;	// skip the effect if the action is nothing
				
			// check for death
			if(effect[i].death)
			{
				effect[i].death--;	// decrement the death counter
				if(effect[i].death==0)
				{
					DMXData[effect[i].channel]=effect[i].min;	// set to min
					effect[i].action=NOTHING;	// don't do anything else for this effect	
				}
			}

			// do tick
			effect[i].tick--;	// decrement tick
			if(effect[i].tick==0)	// check if it is time to do something
			{
				effect[i].tick=effect[i].pause;	// set up the pause
			
				switch(effect[i].action)	// process the action
				{
					case FADE_ON:
					case BOUNCE_ON:
						if(DMXData[effect[i].channel] < (effect[i].max - effect[i].jump))
						{
							DMXData[effect[i].channel]+=effect[i].jump;	
						} else
						{
							DMXData[effect[i].channel]=effect[i].max;	// turn to max
							if(effect[i].action==BOUNCE_ON)
							{
								effect[i].action=BOUNCE_OFF;	// bounce the other way
							}
						}
						break;
						
					case FADE_OFF:
					case BOUNCE_OFF:
						if(DMXData[effect[i].channel] > (effect[i].min + effect[i].jump))
						{
							// DMXData[effect[i].channel]-=effect[i].jump;
							DMXData[effect[i].channel]= DMXData[effect[i].channel] - effect[i].jump;
						} else
						{
							DMXData[effect[i].channel]=effect[i].min;	// turn to min
							if(effect[i].action==BOUNCE_OFF)
							{
								effect[i].action=BOUNCE_ON;	
								effect[i].tick=effect[i].bouncePause;	// hold in min state
							}
						}
						break;
					case PRELOAD_FADE_ON:
						DMXData[effect[i].channel]=effect[i].min;	// start at predefine min
						effect[i].action=FADE_ON;	// now do a normal fade on
						break;
					case PRELOAD_FADE_OFF:
						DMXData[effect[i].channel]=effect[i].max;	// start at predefine max
						effect[i].action=FADE_OFF;	// now do a normal fade off
						break;
										
					case FLICKER:
						if(DMXData[effect[i].channel]==effect[i].max)
						{
							// at max, go to min and wait
							DMXData[effect[i].channel]=effect[i].min;
							effect[i].tick=effect[i].bouncePause;	// go to min for bouncePause duration
						} else
						{
							// at min, go to max for short time
							DMXData[effect[i].channel]=effect[i].max;
							effect[i].tick=effect[i].pause;	// go to max for pause duration
						}	
						break;
				}
			}					
		}

		// ------------------ mirror DMX channels --------------------------------
		// if mirror[x] contains a non-zero value, DMXData[x] = DMXData[mirror[x]]
		for(i=0;i!=sizeof(DMXData);i++)
		{
			if(mirror[i] && mirror[i]<MAXDMX)	// only if non-zero and in bounds
			{
				DMXData[i]=DMXData[mirror[i]];	
			}	
		}
		
		
		
		// implement virtual DMX for the relay pack
		if(oldRelay!=DMXData[VIRTUAL_DMX_RELAY - 1])
		{
			// relay has changed, so send it 
			oldRelay=DMXData[VIRTUAL_DMX_RELAY - 1];	// store so we don't keep sending
			relay(oldRelay);	// set the relay 	
		}

		
		// ----------------- transmit DMX data ------------------------

		// send break manually
		bitclear(PORTB,2);	// drive DMX output low for "break"(once enabled for bit banging)
		RCSTA=0b00000000;	// disable serial port, enable DMX output for bit banging
		TMR6=0;		// start delay
		while(TMR6<92);	// wait 92uS
		bitset(PORTB,2);	// drive DMX output high for "MAB"
		TMR6=0;		// start delay
		while(TMR6<15);	// wait 15uS
		
		// send start code
		RCSTA=0b10000000;	// enable serial port
		TXREG=0x00;			// send start code
		
		// send data
		for(i=0;i!=sizeof(DMXData);i++)
		{
			while(!(PIR1 & 0b10000));	// wait for transmission buffer to empty
			TXREG=DMXData[i];	// send this channel's data
		}		

		
			
		// process state
		switch(state)
	    {
	
	        case IDLE:
	     		         				
				// check for end of countdown
				if(countdownEnded)
				{
					countdownEnded=0;	// clear end of countdown flag
					state=DEFAULT;		// go to default state
					oldIRCommand=0;	// reset oldIRCommand
				} else	if(IRCommand)
				{
					if(oldIRCommand==0b101 && ((IRCommand & 0b11) == 0b10) && (PORTB & 0x1))	// check if prior command was a time command header and this command is a time command and this board is not currently transmitting time commands
					{
						// process RTC time command
						buff=IRCommand & 0b1111;
						switch(buff)
						{
							case 0b0010:	// second low byte
								i=(IRCommand & 0b11110000) >>4;
								tempSecond=(tempSecond & 0b11110000) | i;
								break;
							case 0b0110:	// second high byte
								i=(IRCommand & 0b11110000);
								tempSecond=(tempSecond & 0b00001111) | i;
								break;
							case 0b1010:	// minute low byte
								i=(IRCommand & 0b11110000) >>4;
								tempMinute=(tempMinute & 0b11110000) | i;
								break;
							case 0b1110:	// minute high byte
								i=(IRCommand & 0b11110000);
								tempMinute=(tempMinute & 0b00001111) | i;
								RTC(0x80,tempSecond);	// write updated value to RTC
								RTC(0x82,tempMinute);	// write updated value to RTC
								if(bridgeInstalled)
								{
									second=tempSecond;
									minute=tempMinute;
									writeI2CClock();	// update bridge RTC	
								}	
								state=RTC_WAS_SET;	// so you know the RTC command was completed
								break;
						}
					}
					if(IRCommand!=oldIRCommand)  // don't process duplicates in case the remote button is held down
					{
						switch(IRCommand)
						{
							case POWER:
								state=DEFAULT;
								break;
							case MUTE_BUTTON:
								state=MUTE;
								break;
							case CHANNEL_UP:
								state=CH_PLUS;
								break;
							case CHANNEL_DOWN:
								state=CH_MINUS;
								break;
							case VOLUME_DOWN:
								state=V_MINUS;
								break;
							case VOLUME_UP:
								state=V_PLUS;
								break;
							default:
								break; 
						}
					}
					oldIRCommand=IRCommand;
					IRCommand=0;	// clear IRCommand
				}
				
				// check on the contact closure inputs
				if((PORTA & 0b100)==0)
				{
					// check a second time for glitch
					delayMS(10);	// give input time to change
					if((PORTA & 0b100)==0 && oldContact1==1)  // filter glitch and also make sure contact is not held low
					{
						state=CONTACT1;
						oldContact1=0;	// was low
					}


				} else
				{
					oldContact1=1;	// was high
				}

				if((PORTA & 0b10000000)==0)
				{
					// check a second time for glitch
					delayMS(10);	// give input time to change
					if((PORTA & 0b10000000)==0 && oldContact2==1) // filter glitch and also make sure contact is not held low
					{
						state=CONTACT2;
						oldContact2=0;	// was low
					}
				} else
				{
					oldContact2=1;	// was high
				}
	      
	            break;
	
	
			case CH_PLUS:
	   			
				if(1)
				{
					SendIR(1);	// usb
					SendIR(5); // send "3" to play track 3
					SendIR(2); // send "ok"	
					loopCount=REACT0 + CH_PLUS_HOLDOFF;	// delay relay 
				}
   				
   				
				StateHold=CH_PLUS;			//							
				countdownHold=CH_PLUS_DWELL;	// go to default in X seconds
				countdown=0;	// kill any existing countdown

				
				state=WAIT_BEFORE_RELAY;
   				break;


			case CH_MINUS:
   				
  
				if(1)
				{
					SendIR(1);	// usb
					SendIR(6); // send "4" to play track 4
					SendIR(2); // send "ok"	
					loopCount=REACT0 + CH_MINUS_HOLDOFF;	// delay relay 
				}

   				
   				
				StateHold=CH_MINUS;			// turn on DMX channel 3							
				countdownHold=CH_MINUS_DWELL;	// go to default in X seconds
				countdown=0; // kill any existing countdown

				
				state=WAIT_BEFORE_RELAY;	
   				break;

			case V_PLUS:
   				
  
				SendIR(1);	// usb
				SendIR(12); // send "7" to play track 7
				SendIR(2); // send "ok"	
				loopCount=REACT0 + V_PLUS_HOLDOFF;	// delay relay 
				   				
				StateHold=V_PLUS;			//
				countdownHold=V_PLUS_DWELL;	// go to default in X seconds
				countdown=0; // kill any existing countdown
				
				state=WAIT_BEFORE_RELAY;	
   				break;


			case V_MINUS:
   				
  
				SendIR(1);	// usb
				SendIR(13); // send "8" to play track 8
				SendIR(2); // send "ok"	
				loopCount=REACT0 + V_MINUS_HOLDOFF;	// delay relay 
				   				
				StateHold=V_MINUS;			//
				countdownHold=V_MINUS_DWELL;	// go to default in X seconds
				countdown=0; // kill any existing countdown
				
				state=WAIT_BEFORE_RELAY;	
   				break;


			case MUTE:
   				
				if(1)
				{
					SendIR(1);	// usb
					SendIR(7); // send "5" to play track 5
					SendIR(2); // send "ok"	
					loopCount=REACT0 + MUTE_HOLDOFF;	// delay relay 
				}

   				
   				
				StateHold=MUTE;			// turn on DMX channel 3							
				countdownHold=MUTE_DWELL;	// go to default in X seconds
				countdown=0;	// kill any existing countdown


				state=WAIT_BEFORE_RELAY;	
   				break;



			case WAIT_BEFORE_RELAY:
				if(loopCount==0)
				{
					switch(StateHold)
					{
					
						case CH_PLUS: // success												
							setEffect(CH_PLUS);							
							break;
						case CH_MINUS: // fail
							setEffect(CH_MINUS);
							break;
						case V_PLUS: // success												
							setEffect(V_PLUS);							
							break;
						case V_MINUS: // fail
							setEffect(V_MINUS);
							break;
							
						case MUTE: // hint
							setEffect(MUTE);
							break;
						
					}

			

					IRCommand=0;	// clear command buffer
					countdown=countdownHold;	// kickoff countdown
					state=IDLE;
				}
				break;

			case CONTACT1:
				state=CONTACT1_STATE;
				break;

			case CONTACT2:
				state=CONTACT2_STATE;
				break;
				
			case RTC_WAS_SET:
				
				clearDMX();
				
				// turn on DMX lights so you know the RTC was set
				DMXData[7]=0xff;
				DMXData[8]=0xff;
				DMXData[9]=0xff;
				DMXData[15]=0xff;
				DMXData[16]=0xff;
				DMXData[17]=0xff;
				

				clearEffect();	// clear the effects
				
				countdown=4;	// go to default after countdown

  				state=IDLE;
  	
				break;
  			


  			case BGYAM:
  			
  
				if(1)
				{
					SendIR(1);	// usb
					SendIR(4); // send "2" to play track 2
					SendIR(2); // send "ok"	
				}

				
				//clearDMX();
								
				setEffect(BGYAM);
								
				countdown=62;	// go to default after countdown

  				state=IDLE;
  				break;
  				
/*  			
  			case WARNING:
  				SendIR(1);	// usb
				SendIR(14); // play track 
				SendIR(2); // send "ok"	
								
				countdown=10;	// go to default after countdown

  				state=IDLE;
  				break;
*/
  				
  				

			// perform sound_check loop
			case SOUND_CHECK:
				if(countdown==0)
				{
					if(1)
					{
						SendIR(1);	// usb
						SendIR(8); // send "6" to play track 6
						SendIR(2); // send "ok"	
					}
				
					countdown=15;	// restart track every 15 seconds	
				}
				

				clearDMX();
				// turn on all DMX devices so workers can see
				for(i=0;i!=sizeof(DMXData);i++)	//
				{
					DMXData[i]=0xff;	
				}
				DMXData[VIRTUAL_DMX_RELAY - 1] = 1;	// only turn on the first relay

				clearEffect();	// clear the effects
	
				if(IRCommand)
				{
					state=IDLE;	// go process the IRCommand	
				}
				break;
  
  

  			// turn on default relay and default sound
  			case DEFAULT:
			default:
				if(1)
				{
					SendIR(1);	// usb
					SendIR(3); // send "1" to play track 1
					SendIR(2); // send "ok"	
				}
   				
				//clearDMX();
				
				setEffect(DEFAULT);  // set DMX effects
									
				IRCommand=0;	// clear command buffer
				countdown=0;	// end any existing countdown
				state=IDLE;	// go to idle state
   				break;
		}

	}

} 

